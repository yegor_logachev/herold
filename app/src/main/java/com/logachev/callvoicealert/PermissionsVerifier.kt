package com.logachev.callvoicealert

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.support.v4.content.ContextCompat

/**
 * Created by Yegor on 8/25/17.
 */
class PermissionsVerifier {

    companion object {

        //todo change this implementation to extensions

        private fun isPermissionGranted(context: Context, permission: String) =
                ContextCompat.checkSelfPermission(context, permission) == PackageManager.PERMISSION_GRANTED

        fun isReadContactsPermissionGranted(context: Context) = isPermissionGranted(context, Manifest.permission.READ_CONTACTS)

        fun isReadPhoneStatePermissionGranted(context: Context) = isPermissionGranted(context, Manifest.permission.READ_PHONE_STATE)

        fun isPermissionsGranted(context: Context) = isReadContactsPermissionGranted(context) && isReadPhoneStatePermissionGranted(context)
    }
}