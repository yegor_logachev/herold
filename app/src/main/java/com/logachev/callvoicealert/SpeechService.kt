package com.logachev.callvoicealert

import android.app.Service
import android.content.ContentResolver
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.media.AudioManager
import android.net.Uri
import android.os.IBinder
import android.provider.ContactsContract
import android.speech.tts.TextToSpeech
import java.util.*

/**
 * Created by Yegor on 8/15/17.
 */

const val UNKNOWN_NUMBER = "Unknown number"
const val UNDEFINED_NUMBER = "Undefined number"
const val PHONE_NUMBER_EXTRA_KEY = "phone_number_extra_key"
const val SPEECH_SERVICE_LOG = "SpeechServiceLog"
const val END_OF_SPEECH_ID = "end of speech"

class SpeechService : Service(), TextToSpeech.OnInitListener, TextToSpeech.OnUtteranceCompletedListener {

    lateinit private var textToSpeech: TextToSpeech
    private var speech: String? = null
    private var isInit = false
    private val speechParams: HashMap<String, String> by lazy {
        val params = HashMap<String, String>()
        params.put(TextToSpeech.Engine.KEY_PARAM_STREAM, AudioManager.STREAM_ALARM.toString())
        params.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, END_OF_SPEECH_ID)
        params
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        initTextToSpeech(this)
        val phoneNumber: String? = intent.getStringExtra(PHONE_NUMBER_EXTRA_KEY)
        speech = phoneNumber?.let {
            getContactNameByPhone(contentResolver, it) ?: UNDEFINED_NUMBER
        } ?: UNKNOWN_NUMBER
        trySpeechContactName()
        return super.onStartCommand(intent, flags, startId)
    }

    private fun initTextToSpeech(context: Context) {
        textToSpeech = TextToSpeech(context, this)
    }

    private fun trySpeechContactName() {
        if (!speech.isNullOrEmpty() && isInit) {
            textToSpeech.setOnUtteranceCompletedListener(this)
            textToSpeech.speak(speech, TextToSpeech.QUEUE_FLUSH, speechParams)
        }
    }

    override fun onInit(i: Int) {
        if (i == TextToSpeech.SUCCESS) {
            isInit = true
            trySpeechContactName()
        }
    }

    private fun getContactNameByPhone(contentResolver: ContentResolver, phone: String): String? {
        var name: String? = null
        val lookupUri = Uri.withAppendedPath(ContactsContract.PhoneLookup.CONTENT_FILTER_URI, Uri.encode(phone))
        val cursor: Cursor? = contentResolver.query(lookupUri, arrayOf(ContactsContract.PhoneLookup.DISPLAY_NAME), null, null, null)
        if (cursor != null && cursor.count > 0) {
            cursor.moveToFirst()
            name = cursor.getString(0)
            cursor.close()
        }
        return name
    }

    override fun onBind(intent: Intent): IBinder? = null

    override fun onUtteranceCompleted(s: String) {
        textToSpeech.shutdown()
        stopSelf()
    }

    companion object {

        fun getLaunchIntent(context: Context, phoneNumber: String): Intent {
            val intent = Intent(context, SpeechService::class.java)
            intent.putExtra(PHONE_NUMBER_EXTRA_KEY, phoneNumber)
            return intent
        }
    }
}
