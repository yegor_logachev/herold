package com.logachev.callvoicealert

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.telephony.TelephonyManager

/**
 * Created by Yegor on 7/22/17.
 */

class CallReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        val extras: Bundle? = intent.extras
        if (extras != null && isRinging(extras)) {
            val phoneNumber = getPhoneNumber(extras)
            if (phoneNumber != null && PermissionsVerifier.isReadContactsPermissionGranted(context)) {
                startSpeechService(context, phoneNumber)
            }
        }
    }

    private fun isRinging(extras: Bundle): Boolean {
        val state: String? = extras.getString(TelephonyManager.EXTRA_STATE)
        return TelephonyManager.EXTRA_STATE_RINGING == state
    }

    private fun startSpeechService(context: Context, phoneNumber: String) {
        context.startService(SpeechService.getLaunchIntent(context, phoneNumber))
    }

    private fun getPhoneNumber(extras: Bundle): String? = extras.getString(TelephonyManager.EXTRA_INCOMING_NUMBER)
}
