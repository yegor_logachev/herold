package com.logachev.callvoicealert

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Switch

class MainActivity : AppCompatActivity() {

    private val switch: Switch by lazy {
        findViewById(R.id.switcher) as Switch
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val permissionsGranted = PermissionsVerifier.isPermissionsGranted(this)
        switch.isChecked = permissionsGranted
        if (!permissionsGranted) {
            requestPermissions()
        }
    }

    private fun requestPermissions() {

    }
}
